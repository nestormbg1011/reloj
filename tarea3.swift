class Reloj {
	var horas = 0
	var minutos = 0
	var segundos = 0

	func setReloj(tiempoSegundos: Int) {
		if tiempoSegundos < 3600 {
			horas = 0
			minutos = (tiempoSegundos % 3600) / 60
			segundos = tiempoSegundos - (minutos * 60)
		} else {
			horas = tiempoSegundos / 3600
			minutos = (tiempoSegundos % 3600) / 60
			segundos = tiempoSegundos - (horas * 3600 + minutos * 60)
		}
		
		if horas > 23 {
			horas = 0
			minutos = 0
			segundos = 0
		}
	}

	func setHoras(horas: Int) {
		self.horas = horas
	}

	func setMinutos(minutos: Int) {
		self.minutos = minutos
	}

	func setSegundos(segundos: Int) {
		self.segundos = segundos
	}

	func getHoras() -> Int {
		return horas
	}

	func getMinutos() -> Int {
		return minutos
	}

	func getSegundos() -> Int {
		return segundos
	}

	func tick() {
		let tiempoSegundos = horas * 3600 + minutos * 60 + segundos + 1
		self.setReloj(tiempoSegundos: tiempoSegundos)
	}
	
	func addReloj(relojsito: Reloj) {
		let tiempoSegundos1 = horas * 3600 + minutos * 60 + segundos 
		let tiempoSegundos2 =	relojsito.horas * 3600 + relojsito.minutos * 60 + relojsito.segundos
		self.setReloj(tiempoSegundos: tiempoSegundos1 + tiempoSegundos2)
	}
	
	func toString() {
		var HH = ""
		var MM = ""
		var SS = ""

		if self.horas / 10 == 0 {
			HH = "0"
		} 

		if self.minutos / 10 == 0 {
			MM = "0"
		}  

		if self.segundos / 10 == 0 {
			SS = "0"
		} 

		print("Tiempo en formato [HH:MM:SS]: [\(HH)\(self.getHoras()):\(MM)\(self.getMinutos()):\(SS)\(self.getSegundos())]")
	}

	func tickDecrement() {
		let tiempoSegundos = horas * 3600 + minutos * 60 + segundos - 1
		self.setReloj(tiempoSegundos: tiempoSegundos)
	}
}

let reloj1 = Reloj()
let reloj2 = Reloj()
print("------------Pruebas------------")
reloj1.setReloj(tiempoSegundos: 12489)
reloj2.setReloj(tiempoSegundos: 23478)
print("Tiempo ingresado del Reloj 1: 12489 segundos")
reloj1.toString()
print("Tiempo ingresado del Reloj 2: 23478 segundos")
reloj2.toString()
print("La suma de ambas horas es: ")
reloj1.addReloj(relojsito: reloj2)
reloj1.toString()
print("La hora del Reloj 1 aumentado en 1 segundo es: ")
reloj1.tick()
reloj1.toString()
print("La hora del Reloj 1 disminuido en 1 segundo es: ")
reloj1.tickDecrement()
reloj1.toString()

